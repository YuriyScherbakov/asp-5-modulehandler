﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Home.aspx.cs"  Inherits="ModuleHandler.WebForm1" %>


<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
   
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <script src="Script/ImageScript.js"></script>
</head>
<body>
    <form id="form1" runat="server" >
      
    <div>
        <asp:Label ID="Label1" runat="server" Text="Загрузите изображение на сервер"></asp:Label>
        <a href="Upload.aspx" target="_self">Upload.aspx</a>
        <br />
        <br />
        <asp:Label ID="Label" runat="server" Text="Список изображений, хранящихся на сервере"></asp:Label>
        <asp:DropDownList ID="DropDownListImages" runat="server"></asp:DropDownList>
        <br />
          <br />
       
         <asp:Button ID="ButtonOpenImage" OnClientClick="Show()" runat="server" Text="Открыть в новой вкладке"/>

         <asp:Button ID="ButtonSaveImage" runat="server" OnClientClick="DownLoad()" Text="Скачать на свой ПК"/>
    </div>
        <asp:HiddenField ID="HiddenFieldUserkey" runat="server" />
    </form>
</body>
</html>
