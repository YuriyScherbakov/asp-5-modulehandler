﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Upload.aspx.cs" Inherits="ModuleHandler.Upload"  %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title></title>
    <script src="Script/JavaScript.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:FileUpload ID="FileUpload" runat="server" ValidateRequestMode="Enabled" />
            <asp:Button ID="Button" runat="server" Text="Отправить" OnClick="Button_Click"/>
            <asp:RegularExpressionValidator runat="server" ErrorMessage="Загружать можно только картинки" Display="Dynamic"
                ValidationExpression="(?:([^:/?#]+):)?(?://([^/?#]*))?([^?#]*\.(?:jpg|gif|png))(?:\?([^#]*))?(?:#(.*))?"
                ControlToValidate="fileUpload" ForeColor="Red"></asp:RegularExpressionValidator>
            <asp:RequiredFieldValidator
                ID="RequiredFieldValidator" ForeColor="Red" runat="server" Display="Dynamic"
                ErrorMessage="Вы не выбрали файл"
                ControlToValidate="FileUpload"></asp:RequiredFieldValidator>
            <br />
            <br />

            <asp:Label ID="LabelPicReady" runat="server" Text=""></asp:Label>
            <br />
            <br />

            <asp:Label ID="LabelPicID" runat="server" Text=""></asp:Label>  
             <br />
            <br />
            <asp:Button ID="ButtonBack" runat="server" Text="Вернуться" OnClick="ButtonBack_Click" />
          
        </div>
    </form>
</body>
</html>
