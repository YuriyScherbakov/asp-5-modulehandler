﻿using System;
using System.Web;

namespace ModuleHandler.Modules
{
    public class RedirectToHome : IHttpModule
    {
        /// <summary>
        /// Вам потребуется настроить этот модуль в файле Web.config вашего
        /// веб-сайта и зарегистрировать его с помощью IIS, чтобы затем воспользоваться им.
        /// Дополнительные сведения см. по ссылке: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpModule Members

        public void Dispose()
        {
            //удалите здесь код.
        }

        public void Init(HttpApplication context)
        {

            context.BeginRequest += Context_BeginRequest;
        }

        private void Context_BeginRequest(object sender,EventArgs e)
        {
            HttpApplication currentApplication =  (HttpApplication)sender ;
            string filePath = currentApplication.Request.AppRelativeCurrentExecutionFilePath;
            if ( filePath == "~/")
            {
                currentApplication.Response.Redirect("~/Home.aspx");
            }
        }

        #endregion

        public void OnLogRequest(Object source,EventArgs e)
        {
            //здесь можно разместить логику занесения данных
        }
    }
}
