﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace ModuleHandler.Modules
{
    public class SecurityModule : IHttpModule
    {
        /// <summary>
        /// Вам потребуется настроить этот модуль в файле Web.config вашего
        /// веб-сайта и зарегистрировать его с помощью IIS, чтобы затем воспользоваться им.
        /// Дополнительные сведения см. по ссылке: http://go.microsoft.com/?linkid=8101007
        /// </summary>

        public static string md5UserKey = GetMd5Hash(UserKey);

        public void Dispose()
        {
            //удалите здесь код.
        }

       static string UserKey
        {
            get
            {
                return ConfigurationManager.AppSettings ["Userkey"];
            }
        }

        public void Init(HttpApplication context)
        {

            context.PreRequestHandlerExecute += Context_BeginRequest;



        }

        void NotAuthorizedResponse( HttpApplication sender, string md5UserKey)
        {
                    ( (HttpApplication)sender ).Response.Write("You are not authorized");
                    ( (HttpApplication)sender ).Response.Write("<br><br>");
                    ( (HttpApplication)sender ).Response.Write("Для удобства проверки");
                    ( (HttpApplication)sender ).Response.Write("<br><br>");
                    ( (HttpApplication)sender ).Response.Write("?UserKey=" + md5UserKey);
                    ( (HttpApplication)sender ).Response.Flush();
                    ( (HttpApplication)sender ).Response.End();

        }
        private void Context_BeginRequest(object sender,EventArgs e)
        {
           

            if ( ( (HttpApplication)sender ).Request.Params ["UserKey"] != null )
            {

                string md5UserKeyParams = ( (HttpApplication)sender ).Request.Params ["UserKey"];

                StringComparer stringComparer = StringComparer.OrdinalIgnoreCase;
                if ( stringComparer.Compare(md5UserKeyParams,md5UserKey) == 0 )
                {
                    try
                    {

                        ( (HttpApplication)sender ).Session ["isAuthorized"] = true;
                        ( (HttpApplication)sender ).Session.Timeout = 1;
                    }
                    catch { }
                    return;


                }
                else
                {
                    NotAuthorizedResponse((HttpApplication)sender,md5UserKey);
                }

            }
            else
            {
                NotAuthorizedResponse((HttpApplication)sender,md5UserKey);
            }

        }

       static string GetMd5Hash(string UserKey)
        {
            MD5 md5Hash = MD5.Create();

            // Convert the input string to a byte array and compute the hash.
            byte [] data = md5Hash.ComputeHash(Encoding.UTF8.GetBytes(UserKey));

            // Create a new Stringbuilder to collect the bytes
            // and create a string.
            StringBuilder sBuilder = new StringBuilder();

            // Loop through each byte of the hashed data 
            // and format each one as a hexadecimal string.
            for ( int i = 0; i < data.Length; i++ )
            {
                sBuilder.Append(data [i].ToString("x2"));
            }

            // Return the hexadecimal string.
            return sBuilder.ToString();
        }




        public void OnLogRequest(Object source,EventArgs e)
        {
            //здесь можно разместить логику занесения данных
        }
    }
}
