﻿using System;
using System.Web;

namespace ModuleHandler.Modules
{
    public class MD5Adder : IHttpModule
    {
        /// <summary>
        /// Вам потребуется настроить этот модуль в файле Web.config вашего
        /// веб-сайта и зарегистрировать его с помощью IIS, чтобы затем воспользоваться им.
        /// Дополнительные сведения см. по ссылке: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpModule Members

        public void Dispose()
        {
            //удалите здесь код.
        }

        public void Init(HttpApplication context)
        {
            // Ниже приводится пример обработки события LogRequest и предоставляется 
            // настраиваемая реализация занесения данных
            context.PreRequestHandlerExecute += Context_PreRequestHandlerExecute;
        }

        private void Context_PreRequestHandlerExecute(object sender,EventArgs e)
        {
            HttpApplication currentApplication = (HttpApplication)sender;

            if ( currentApplication.Request.CurrentExecutionFilePathExtension == ".js" )
            {
                currentApplication.Server.Transfer(currentApplication.Request.Path +
                                "?UserKey=" + ModuleHandler.Modules.SecurityModule.md5UserKey,true);
            }

            if ( currentApplication.Context.Session != null )
            {
                if ( ( currentApplication.Context.Session ["isAuthorized"] ) != null )
                {
                    if ( currentApplication.Request.Params ["UserKey"]  == null )
                    {
                       
                       currentApplication.Response.Redirect(currentApplication.Request.Path +
                            currentApplication.Request.QueryString +
                                "?UserKey=" + ModuleHandler.Modules.SecurityModule.md5UserKey,true);
                    }
                   
                      
                }

            }


        }

        #endregion

        public void OnLogRequest(Object source,EventArgs e)
        {
            //здесь можно разместить логику занесения данных
        }
    }
}
