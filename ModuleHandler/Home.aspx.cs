﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ModuleHandler
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        public void Page_PreRender(object sender,EventArgs e)
        {
            if ( string.IsNullOrEmpty(this.Page.Form.Action) )
                this.Page.Form.Action = "Home.aspx";
        }

        protected void Page_Load(object sender,EventArgs e)
        {
            this.HiddenFieldUserkey.Value = "&UserKey=" + Modules.SecurityModule.md5UserKey;
            if ( !IsPostBack )
            {

                DataSet Images = new DataSet("Images");
                Images.ReadXml(Server.MapPath("~/App_Data/ImagesXML.xml"));
                this.DropDownListImages.DataSource = Images;
                this.DropDownListImages.DataTextField = "ImagePath";
                this.DropDownListImages.DataBind();
            }
        }
    }
}