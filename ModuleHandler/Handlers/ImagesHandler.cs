﻿using System;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Web;

namespace ModuleHandler.Handlers
{
    public class ImagesHandler : IHttpHandler
    {

        System.Drawing.Image image;
        /// <summary>
        /// Вам потребуется настроить этот обработчик в файле Web.config вашего 
        /// веб-сайта и зарегистрировать его с помощью IIS, чтобы затем воспользоваться им.
        /// Дополнительные сведения см. по ссылке: http://go.microsoft.com/?linkid=8101007
        /// </summary>
        #region IHttpHandler Members

        public bool IsReusable
        {
            // Верните значение false в том случае, если ваш управляемый обработчик не может быть повторно использован для другого запроса.
            // Обычно значение false соответствует случаю, когда некоторые данные о состоянии сохранены по запросу.
            get
            {
                return true;
            }
        }

        public void ProcessRequest(HttpContext context)
        {
            DownLoadImage("Images/" + context.Request.Params ["ImagePath"]);
            AddWaterMarkOnImage();


            if ( context.Request.Params ["toDo"] == "DownLoad" )
            {
                HttpContext.Current.Response.ContentType = "application/octet-stream";
                HttpContext.Current.Response.AddHeader("Content-Disposition",
                "attachment; filename=" + context.Request.Params ["ImagePath"]);
                context.Response.OutputStream.Write(
                ConvertImageIntoByteArray(),0,ConvertImageIntoByteArray().Length);
                HttpContext.Current.Response.End();
            }
            else
            {
                context.Response.ContentType = "image/jpeg";
                context.Response.BinaryWrite(ConvertImageIntoByteArray());
               
                context.Response.End();
            }


        }

        byte [] ConvertImageIntoByteArray()
        {
            Stream stream = new MemoryStream();
            image.Save(stream,ImageFormat.Jpeg);
            stream.Position = 0;
            BinaryReader binaryReader = new BinaryReader(stream);

            byte [] ImageBinary = new byte [stream.Length];

            ImageBinary = binaryReader.ReadBytes((int)stream.Length);

            stream.Close();
            binaryReader.Close();

            return ImageBinary;

        }

        void AddWaterMarkOnImage()
        {
          
            System.Drawing.Graphics graphics = System.Drawing.Graphics.FromImage(this.image);
            graphics.DrawString(
                ConfigurationManager.AppSettings ["watermark"],
                new System.Drawing.Font("Calibri",image.Width/10),
                System.Drawing.Brushes.Azure,image.Width / 50,
                    image.Height / 2);
        }

        void DownLoadImage(string ImagePath)
        {
            try
            {

                this.image = System.Drawing.Image.FromFile
                    (HttpContext.Current.Server.MapPath("~/" + ImagePath));

            }
            catch ( Exception ex )
            {

            }

        }

        #endregion
    }
}
