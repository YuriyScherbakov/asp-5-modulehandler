﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;

namespace ModuleHandler
{
    public partial class Upload : System.Web.UI.Page
    {


        protected void Page_Load(object sender,EventArgs e)
        {
            if ( IsPostBack  )
            {
                this.LabelPicReady.Text = "";
                this.LabelPicID.Text = "";
            }
        }

        protected void Button_Click(object sender,EventArgs e)
        {
            if ( !IsValid )
            {
                return;
            }

            if ( FileUpload.HasFile )
            {

                try
                {
                    Guid imgID = Guid.NewGuid();
                    FileUpload.SaveAs(Server.MapPath("~/Images/" + FileUpload.FileName));
                    DataSet Images = new DataSet("Images");
                    Images.ReadXml(Server.MapPath("~/App_Data/ImagesXML.xml"));
                    Images.Tables["Image"].Rows.Add(FileUpload.FileName,imgID);
                    Images.WriteXml(Server.MapPath("~/App_Data/ImagesXML.xml"));
                    this.LabelPicReady.Text = "Изображение сохранено на сервере";
                    this.LabelPicID.Text = "ID рисунка  " + imgID;
                }
                catch ( Exception ex )
                {
                    this.LabelPicReady.Text = "Не удалось загрузить изображение";
                    this.LabelPicID.Text = "";
                }
            }
        }

        protected void ButtonBack_Click(object sender,EventArgs e)
        {
            Response.Redirect("Home.aspx"+"?UserKey=" + ModuleHandler.Modules.SecurityModule.md5UserKey);
        }
    }
}